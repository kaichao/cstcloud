package cn.cstcloud;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * 数据导入
 */
public class DataImporter {
    public static void main(String[] args) throws Exception{
        // 通过管理程序，生成组织表（org_table）的记录，查找org_id
        // 通过管理程序，生成订单表(order_table)的记录、组织资源表（resource_table）的记录

        Files.lines(new File("data/earth.txt").toPath())
                .forEach( s -> {
                    String userId="", orgName="", userName="", mobile="", email="";
                    String[] ss = s.trim().split("[\t ]");
                    switch (ss.length){
                        case 5: email = ss[4];
                        case 4: mobile = ss[3];
                        case 3: userName = ss[2];
                        case 2: orgName = ss[1];
                        case 1: userId = ss[0];
                    }
                    try {
                        writeTo(userId, orgName, userName, mobile, email);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
    }

    private static String url = "jdbc:mysql://159.226.245.252:3306/cstcloud?" +
            "useUnicode=true&amp;characterEncoding=gbk&amp;autoReconnect=true&amp;failOverReadOnly=false";
    private static void writeTo(String userId, String orgName, String userName, String mobile, String email) throws Exception{
        // 通过数据表查找
        String parentResourceId = "25";
        String orgId = "40";
        String sql1="INSERT INTO account_table_new(uid,email,name,mobile,dep_org) VALUES(?,?,?,?,?)";
        String sql2="INSERT INTO org_account_table_new(org_id,account_id) VALUES(?,?)";
        String sql3="INSERT INTO sub_resource_table(parent_id,account_id,params,valid) VALUES(?,?,'1000000','Y')";


        Class.forName("com.mysql.jdbc.Driver");
        try (Connection c =DriverManager.getConnection(url,"cstcloud","cstcloud")) {
            // 插入账号表（account_table）的记录
            PreparedStatement ps1 = c.prepareStatement(sql1);
            ps1.setString(1,userId);
            ps1.setString(2,email);
            ps1.setString(3,userName);
            ps1.setString(4,mobile);
            ps1.setString(5,orgName);
            ps1.executeUpdate();
            ps1.close();
            // 插入组织账号表（org_account_table）的记录
            PreparedStatement ps2 = c.prepareStatement(sql2);
            ps2.setString(1,orgId);
            ps2.setString(2,userId);
            ps2.executeUpdate();
            ps2.close();

            // 插入用户资源表（sub_resource_table）的记录
            PreparedStatement ps3 = c.prepareStatement(sql3);
            ps3.setString(1,parentResourceId);
            ps3.setString(2,userId);
            ps3.executeUpdate();
            ps3.close();
        }
    }
}
