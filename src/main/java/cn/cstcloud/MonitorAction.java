package cn.cstcloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * 监控操作的封装
 */
public class MonitorAction{
    private static final Logger logger = LoggerFactory.getLogger(MonitorAction.class);

    public static void main(String[] args) throws Exception{
        new MonitorAction().run();
    }

    void run() throws Exception{
        String url = "jdbc:mysql://localhost:3306/cstcloud?" +
                "useUnicode=true&amp;characterEncoding=gbk&amp;autoReconnect=true&amp;failOverReadOnly=false";
        //  获取账号记录表account_table中所有账号列表
        String selectSql = "SELECT uid,account_id FROM sub_resource_table";
        // 插入监控记录
        String insertSql = "INSERT INTO storage_stat_table(sub_resource_id,dt,bytes_num,file_num) VALUES(?,?,?,?)";
        Class.forName("com.mysql.jdbc.Driver");
        try (Connection c = DriverManager.getConnection(url,"cstcloud","cstcloud")) {
            PreparedStatement ps = c.prepareStatement(insertSql);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(selectSql);
            while(rs.next()){
                String subResourceId =rs.getString(1);
                String accountId = rs.getString(2);
                logger.debug("sub resource id:{}, account id:{}",subResourceId,accountId);
                AccountObj.DataItem item = AccountObj.select(accountId);
                ps.setString(1,subResourceId);
                ps.setString(2,item.getDt());
                ps.setLong(3,item.getUsedMb());
                ps.setLong(4,item.getUsedInodes());
                ps.executeUpdate();
            }
            rs.close();
            s.close();
            ps.close();
        }
    }
}