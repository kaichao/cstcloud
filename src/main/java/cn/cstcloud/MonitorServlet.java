package cn.cstcloud;

import javax.servlet.http.HttpServlet;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 监控操作的Servlet封装
 */
public class MonitorServlet extends HttpServlet{
    @Override
    public void init(){
        // 每天更新一次文件列表
        Executors.newScheduledThreadPool(1)
                .scheduleAtFixedRate(
                        () -> doMonitor(),
                        1,
                        86400,
                        TimeUnit.SECONDS);
    }

    private void doMonitor(){
        try {
            new MonitorAction().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}